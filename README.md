[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/thollarf/comp_ratio/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/thollarf/comp_ratio/commits/master)

# comp_ratio

## Dependencies

Python 3.6 with the following packages:
- numpy < 1.17
- obspy

## Install
We recommend to create a virtual env: 
```bash
python3 -m venv $(pwd)/comp_ratio_venv
```

Then to activate it:

```bash
source $(pwd)/comp_ratio_venv/bin/activate
```

Then to install the dependencies:
```bash
pip3 install -r requirements.txt
```

In certain contexts this can fail as obspy is installed before numy. We then 
recommand to do the installation in two steps:

```bash
xargs -n 1 -L 1 pip3 install < requirements.txt
```
