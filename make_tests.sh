#!/bin/sh

cd ground_truth

for f in `ls` ; do
  echo -n "$f : "
  if $(cmp -s $f ../$f) ; then
    echo "OK"
  else
    echo "KO"
    echo "$f and ../$f differs :"
    echo "=== $f ==="
    cat $f
    echo "=== ../$f ==="
    cat ../$f
    cmp -bl $f ../$f
    exit 1
  fi
done
