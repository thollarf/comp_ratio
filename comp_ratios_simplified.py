###############################################################################
#    This file contains a tool for seismic data quality control               #
#    The method is using component energy ratios to                           #
#    detect metadata and instrument problems of seismic stations.             #
#                                                                             #
#    Copyright (C) 2019 Helle Pedersen                                        #
#    helle.pedersen@univ-grenoble-alpes.fr                                    #
#    If you use this software, either as is or in a modified form,            #
#    please refer to the following publication:                               #
#    Pedersen, H. A., N. Leroy, D. Zigone, M. Vallée, A. T. Ringler           #
#    and Wilson, D. C., 2019. Using component ratios to detect metadata       #
#    and instrument problems of seismic stations: examples from 18 years      #
#    of GEOSCOPE data. Seismological Research, VOL, PAGES tbc                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program. If not, see <https://www.gnu.org/licenses/>.    #
#                                                                             #
#    The ISTerre geodata team, and in particular Franck Thollard, Jérome      #
#    Touvier, Loic Huder and Albanne Lecointre provided precious support in   #
#    turning the code into a publishable form.                                #
###############################################################################

###############################################################################
"""
 The program below makes it possible to reproduce the figures of the publication
 cited above. It is based on the processing explained in the publication.
 The default values correspond to producing the outputs necessary to plot
 Figures 6-8.

 The main limitations of the program are the following:
 - no attempt has been made for a truly thorough clearing up of the code. It
   was originally aimed at my own research. Therefore the user is expected to
   adapt the code to her/his own needs.
 - the program contains many comments, I recommend to read the program carefully to
   understand limitations.
 - only the longest data segment of the day is analysed. Substantial changes
   will need to be made to extend the analysis to all time windows in the day
 - the program calculates component ratios in 8 frequency bands. The number of
   frequency bands and the frequency limites are hardcoded.
 - The program works for the BH channel. It should also work for any other channel
   but if a low frequency channel is chosen, the frequency filters need to be adapted.
   The present frequency filters go up to 5 Hz, so a frequency sampling <20 Hz should not
   be used. However, BH and HH channels, + strong motion channels should work.
-  The program uses either ENZ coordinate system (default) or 12Z coordinate system.
   If other coordinate systems (for example 123 or 23Z) are used, the program should be
   adapted accordingly
-  The program does not check availability of data. It is at present up to the user
   to know which time periods have which channels. The availability webservice of the
   FDSN should soon be available at most FDSN datacenters, and this evolution of the
   programme would be a natural next step.
 - the program aims at analysing data from a single station, with one data request
   per day. Substantial modifications are needed if the user wants to download other
   lengths of data.
 - the program is set up to proceed also if the response removal does not work. This can
   be useful because the response is often close to identical on different components.
   In that case, the program emits a warning. If the instrument response is successfully
   removed, the output file will contain a 1 under the response, otherwise 0.

OUTPUT:
The program will output 2 files:
-  Energy file of the name network_channel_location_station_Energy.txt (eg. G_BHE_00_ECH_Energy.txt)
   It contains 27 columns, with column 1=year, column2=Julian day within the year, column 3-5 total energy
   in frequency band 1 for components E,N,Z, column 6-8 total energy in frequency band 2 for
   components E,N,Z etc. Column 27 contains the value 1 if the instrument response removal
   was successful, 0 if not.
-  Energy ratio file of the name network_channel_location_station_Energy_Ratio.txt (e.g.
   G_BHE_00_ECH_Energy_Ratio.txt. It contains 27 columns. Columns 1, 2 and 27 are identical to those
   of the Energy file. Column 3-5 contains the daily median of the E/Z, N/Z and E/N values calculated in each time
   window for frequency band 1. The next 3 columns correspond to frequency band 2 etc.

"""
import argparse
import logging
from calendar import isleap

import numpy as np
import obspy.core as oc
from obspy.clients.fdsn import Client
# import sys

logging_translate = [
    logging.CRITICAL,
    logging.ERROR,
    logging.WARNING,
    logging.INFO,
    logging.DEBUG,
]


def read_data(
    client,
    network,
    station,
    tbegin,
    tend,
    channelVERT,
    channelHOR1,
    channelHOR2,
    location,
    duration_min,
    long_only,
):
    st = None
    argsZ = (network, station, location, channelVERT, tbegin, tend)
    argsN = (network, station, location, channelHOR2, tbegin, tend)
    argsE = (network, station, location, channelHOR1, tbegin, tend)
    kwargs = {"minimumlength": duration_min, "longestonly": long_only}

    try:
        stZ = client.get_waveforms(*argsZ, **kwargs)
        stN = client.get_waveforms(*argsN, **kwargs)
        stE = client.get_waveforms(*argsE, **kwargs)
        st = stN + stE + stZ
    except Exception as ex:
        logger.warning(f"fail to get all data from {station}: {str(ex)}")
    return st


parser = argparse.ArgumentParser(description=("Comp ratios "))
parser.add_argument(
    "-v",
    type=int,
    default=3,
    help="set logging level: 0 critical, 1 error, 2 warning, 3 info, 4 debug, default info",
)
parser.add_argument(
    "-i", type=str, default="RESIF", help="Client to query (default RESIF)"
)
parser.add_argument("-n", type=str, default="G", help="netw (default G)")
parser.add_argument("-s", type=str, default="ECH", help="stat (default ECH)")
parser.add_argument("-c", type=str, default="BH", help="channel (default BH)")
parser.add_argument(
    "-p", type=str, default="E", help="component (default E, alternative is 1)"
)
parser.add_argument(
    "-y",
    type=str,
    default="2015:2015",
    help="year to consider (format yearstat:yearend, default 2015:2015",
)
parser.add_argument(
    "-l", type=str, default="00", help="station parameter: location (default 00)"
)
parser.add_argument(
    "-d", type=int, default="83000", help="duration_min in secs (default 83000)"
)
parser.add_argument(
    "--time_window", type=int, default=300, help="time window in sec (default 300)"
)
parser.add_argument("--test", action="store_true", help="switch to test mode.")

args = parser.parse_args()

logging.basicConfig(
    level=logging_translate[args.v],
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)
logger = logging.getLogger(__name__)


# ########################################################################################"
#  INPUT PARAMETER SECTION

test_mode = False
if args.test:
    logger.warning("############## switching to test mode ################")
    client_name = "RESIF"
    netw = "G"
    stat = "ECH"
    location = "00"
    cha = "BH"
    comp = "E"
    twin = 300
    duration_min = 83000
    date_range = "2015:2015"
    test_mode = True
else:
    # station and channel informations.
    client_name = args.i
    netw = args.n
    stat = args.s
    location = args.l
    cha = args.c
    comp = args.p
    if comp not in ("E", "1"):
        logger.error(f"\nRequire E or 1 for the component argument (-c option)")
        exit(1)
    twin = args.time_window  # window length in seconds.
    duration_min = args.d  # only analyse data with more than duration_min seconds
    date_range = args.y

chann = cha + str("?" if len(cha) == 2 else "")
channelHOR1 = cha + comp
channelHOR2 = cha + str("2" if comp == "1" else "N")
channelVERT = cha + "Z"

# Time parameters
# t0 is the reference time - is just a fixpoint in time
# t0 = oc.UTCDateTime("2001-01-01:T00:00:00")
step = 1  # we will get data in 1 day steps. NOTE: no tests done on other steps
stepduration = step * 86400  # step in seconds

# number of windows in a day. Replace int by round if you accept overlapping window around the step value (here: midnight)
nwin = int(stepduration / twin)

#  other time request parameters
# request only the longest segment in the day
long_only = True
yearstart, yearend = (int(x) for x in date_range.split(":"))
iyears = list(range(yearstart, yearend + 1))

logger.info(
    (
        f"\nrequest param: \nclient {client_name} network {netw} station {stat} location {location} "
        f"\nchannels {channelHOR1} {channelHOR2} {channelVERT}"
        f"\nlong_only {long_only} duration_min {duration_min} time window {twin}"
        f"\ntime range {yearstart} -> {yearend} "
    )
)

# define FDSN web service data center
client = Client(client_name)

########################################################################################
#  FILE MANAGEMENT
########################################################################################

# setting output file names and open files. The output file is in the form network_channel_location_station.txt
txtout1 = netw + "_" + channelHOR1 + "_" + location + "_"
txtout3 = ".txt"

outfileEnergy = txtout1 + stat + "_Energy" + txtout3
outfileRatio = txtout1 + stat + "_Energy_Ratio" + txtout3

logger.info(f"outfile Energy={outfileEnergy}    Ratio={outfileRatio}")

########################################################################################


# recover instrument response for the station for 2001-2030 # note that it is faster to write network inventory to disk and use it for all data processing
logger.debug("querying for inventory")
inv = client.get_stations(
    network=netw,
    station=stat,
    channel=chann,
    starttime=oc.UTCDateTime(2001, 1, 1),
    endtime=oc.UTCDateTime(2030, 1, 1),
    level="response",
)

# loop over years
for iyear in iyears:
    timestring = str(iyear) + "-01-01:T00:00:00"
    t_start = oc.UTCDateTime(timestring)
    if test_mode:
        idates = range(0, 3)
    else:
        idates = range(0, 366 if isleap(iyear) else 365, step)
    # loop over days in the year
    for idate in idates:
        tbegin = t_start + idate * 86400
        tend = tbegin + stepduration
        logger.info(f"managing date {tbegin} -> {tend}")
        logger.debug(f"querying {args.i}")
        st = read_data(
            client,
            netw,
            stat,
            tbegin,
            tend,
            channelVERT,
            channelHOR1,
            channelHOR2,
            location,
            duration_min,
            long_only,
        )
        if st is None:
            logger.warning(f"fail to gather data for date {tbegin} -> {tend}")
        else:
            logger.debug(f"processing answer")
            # NOTE no tests to check if fe sampling different on different channels.
            fe = 1.0 / st[0].stats.delta
            # check start time on the first comp, can be improved by taking max of all comps
            sizelimit = (0.98 * twin) * fe
            t = oc.UTCDateTime(st[0].stats.starttime)
            # check end time on the first comp, can be improved by taking min of all comps
            t_end = oc.UTCDateTime(st[0].stats.endtime)

            # standard preprocessing: dmean, dtrend, filter, remove instrument and taper. Rotate if necessary.
            if (t_end - t) > duration_min:
                st0 = st.copy()
                st0.detrend(type="demean")
                st0.detrend(type="linear")
                st0.filter("bandpass", freqmin=0.01, freqmax=fe / 3.0)
                RespFlag = (
                    0
                )  # RespFlag changes to 1 when the instrument response is succesfully removed. It is written in output, allowing to check if pbs.
                try:
                    st0.remove_response(
                        inventory=inv, taper=True, water_level=60, plot=False
                    )
                    RespFlag = 1
                except Exception as ex:
                    logger.warning(f"remove_response failed: {str(ex)}")
                if channelHOR1[2] == "1":
                    st0.rotate("->ZNE", inventory=inv)

                ############################################################
                # filtering in 8 frequency bands. Hard coded for the time being.

                enE = [[] for i in range(8)]
                enN = [[] for i in range(8)]
                enZ = [[] for i in range(8)]
                st_f = [st0.copy() for i in range(8)]
                freqs = [0.01, 0.02, 0.05, 0.1, 0.2, 0.4, 1.0, 2.5, 5.0]
                for k in range(8):
                    st_f[k].filter("bandpass", freqmin=freqs[k], freqmax=freqs[k + 1])

                # loop over all the time windows in the day
                for dt in range(0, nwin * step):
                    filtN = [[] for i in range(8)]
                    filtE = [[] for i in range(8)]
                    filtZ = [[] for i in range(8)]
                    st_trim = [st_f[i].copy() for i in range(8)]

                    # extract the time window analysed with the trim command
                    for k in range(8):
                        st_trim[k].trim(
                            t + dt * twin,
                            t + dt * twin + (twin - 0.01),
                            nearest_sample=False,
                        )

                    # organise the time windows into numpy matrices with all frequency bands, one matrix per component.
                    if st_trim[2] and st_trim[2][0].stats.npts > int(0.98 * twin):
                        for k in range(8):
                            filtN[k] = st_trim[k].select(component="N")[0].data
                            filtE[k] = st_trim[k].select(component="E")[0].data
                            filtZ[k] = st_trim[k].select(component="Z")[0].data

                        # calculate energy on each component as the sum of the squared signal, divided by the number of samples
                        Nsize = np.size(filtN[2])
                        Esize = np.size(filtE[2])
                        Zsize = np.size(filtZ[2])

                        if all(sz > sizelimit for sz in (Nsize, Esize, Zsize)):
                            for k in range(8):
                                enN[k].append(np.sum(np.square(filtN[k])) / Nsize)
                                enE[k].append(np.sum(np.square(filtE[k])) / Esize)
                                enZ[k].append(np.sum(np.square(filtZ[k])) / Zsize)
                            nn = len(enN[2]) - 1

            if nn > 5:
                logger.debug(f"writing data for date {tbegin}")
                # nsecs = "%8d" % (np.rint((tbegin - t0) / 86400) + 1)
                time = "%s %s" % (iyear, idate + 1)

                # output of energy
                row = [time]
                for j in range(8):
                    tmp = [np.median(enE[j]), np.median(enN[j]), np.median(enZ[j])]
                    row.extend(["%9.3e" % val for val in tmp])
                row = row + ["%1d\n" % RespFlag]
                with open(outfileEnergy, "a") as f:
                    f.write(" ".join(row))

                # output of energy ratios
                row = [time]
                for j in range(8):
                    tmp = [
                        np.median(np.divide(enE[j], enZ[j])),
                        np.median(np.divide(enN[j], enZ[j])),
                        np.median(np.divide(enE[j], enN[j])),
                    ]
                    row.extend(["%9.3e" % val for val in tmp])
                row = row + ["%1d\n" % RespFlag]
                with open(outfileRatio, "a") as fratio:
                    fratio.write(" ".join(row))
